<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\StockBarangController;
use App\Http\Controllers\Api\OptionalChallengeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('stock-barang')->group(function (){
    Route::get('/', [StockBarangController::class, 'get']);
    Route::post('/', [StockBarangController::class, 'store']);
    Route::put('/{id_barang}', [StockBarangController::class, 'update']);
    Route::delete('/{id_barang}', [StockBarangController::class, 'destroy']);
});

Route::prefix('purchase-of-goods')->group(function (){
    Route::post('/', [StockBarangController::class, 'purchase']);
});

Route::prefix('sale-of-goods')->group(function (){
    Route::post('/', [StockBarangController::class, 'sale']);
});
