<?php

namespace App\Models\Barang;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockBarang extends Model
{
    use HasFactory;

    protected $table = 'stock_barang';
    protected $primaryKey = 'id_barang';
    protected $fillable  = ['nama_barang','harga_barang','stock_barang'];
    public $timestamps = false;
}
