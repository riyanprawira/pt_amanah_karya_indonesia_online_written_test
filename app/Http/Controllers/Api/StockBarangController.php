<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Barang\StockBarang;

class StockBarangController extends Controller
{
    public function purchase(Request $request)
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            'id_barang' => 'required',
            'stock_barang' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['success' => 0, 'message' => 'Validation Error', 'errors' => $validator->errors()], 400);
        }

        try{
            DB::beginTransaction();
            try{
                $store_stock = StockBarang::where('id_barang', $post['id_barang'])->first();

                if ($store_stock == true) {
                    $store_stock->stock_barang = $store_stock->stock_barang + $post['stock_barang'];
                    $store_stock->save();
                } else {
                    return response()->json(['success' => 0, 'message' => 'Failed to add stock barang, ID barang not found.'], 404);
                }

                DB::commit();

                $response = array(
                    "success"   => 1,
                    "message"   => 'Stock barang successfully added.'
                );

                return response()->json($response, 201);

            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
            }
        }catch(\Exception $e){
            return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
        }
    }

    public function sale(Request $request)
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            'id_barang' => 'required',
            'stock_barang' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['success' => 0, 'message' => 'Validation Error', 'errors' => $validator->errors()], 400);
        }

        try{
            DB::beginTransaction();
            try{
                $store_stock = StockBarang::where('id_barang', $post['id_barang'])->first();

                if ($store_stock == true) {
                    $store_stock->stock_barang = $store_stock->stock_barang - $post['stock_barang'];
                    $store_stock->save();
                } else {
                    return response()->json(['success' => 0, 'message' => 'Failed to sale barang, ID barang not found.'], 404);
                }

                DB::commit();

                $response = array(
                    "success"   => 1,
                    "message"   => 'Stock barang successfully sale.'
                );

                return response()->json($response, 201);

            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
            }
        }catch(\Exception $e){
            return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
        }
    }

    public function get(Request $request)
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            'pages' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['success' => 0, 'message' => 'Validation Error', 'errors' => $validator->errors()], 400);
        }

        $search = [];
        $limit = 10;

        if(strlen($request->get("limit")) > 0){
            $limit = $request->get("limit");
        }

        $page = $request->get("pages");             
        $ofset = ($page - 1) * $limit;
        $param = $request->except("pages","limit");

        if(count($param) > 0){
            foreach($param as $key => $value){
                if(!empty($value)){
                    array_push($search, [$key, 'LIKE', '%'.$value.'%']);
                }
            }
        }

        $items = StockBarang::where($search)->orderBy('nama_barang', 'ASC');
        $data = $items->skip($ofset)->limit($limit)->get();
        $total = $items->count();

        if($total > 0){
            return response()->json(['success' => 1, 'message' => 'Data stock barang found.', 'total_data' => $total, 'data' => $data], 200);
        } else {
            return response()->json(['success' => 0, 'message' => 'Data stock barang not found.', 'total_data' => $total, 'data' => []], 404);
        }
    }

    public function store(Request $request)
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            'nama_barang' => 'required|unique:stock_barang,nama_barang|max:1500',
            'harga_barang' => 'required',
            'stock_barang' => 'required|int'
        ]);

        if($validator->fails()) {
            return response()->json(['success' => 0, 'message' => 'Validation Error', 'errors' => $validator->errors()], 400);
        }

        try{
            DB::beginTransaction();
            try{
                $store = StockBarang::create($post);

                if ($store != true) {
                    return response()->json(['success' => 0, 'message' => 'Failed to add stock barang.'], 500);
                }

                DB::commit();

                $response = array(
                    "success"   => 1,
                    "message"   => 'Stock barang successfully added.'
                );

                return response()->json($response, 201);

            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
            }
        }catch(\Exception $e){
            return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request, $id_barang)
    {
        $post = $request->all();
        $update = StockBarang::find($id_barang);

        $validator = Validator::make($post, [
            'nama_barang' => 'required|unique:stock_barang,nama_barang,'.$update->id_barang.',id_barang|max:1500',
            'harga_barang' => 'required',
            'stock_barang' => 'required|int'
        ]);

        if($validator->fails()) {
            return response()->json(['success' => 0, 'message' => 'Validation Error', 'errors' => $validator->errors()], 400);
        }

        try{
            DB::beginTransaction();
            try{
                if ($update == true) {
                    $update->nama_barang = $post['nama_barang'];
                    $update->harga_barang = $post['harga_barang'];
                    $update->stock_barang = $post['stock_barang'];
                    $update->save();
                } else {
                    return response()->json(['success' => 0, 'message' => 'ID not found, failed to update stock barang.'], 500);
                }

                DB::commit();

                $response = array(
                    "success"   => 1,
                    "message"   => 'Stock barang successfully updated.'
                );

                return response()->json($response, 201);

            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
            }
        }catch(\Exception $e){
            return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
        }
    }

    public function destroy($id_barang)
    {
        try{
           DB::beginTransaction();
           try{
                $delete = StockBarang::find($id_barang);

                if ($delete == true) {
                    $delete->delete();
                } else {
                    return response()->json(['success' => 0, 'message' => 'ID not found, failed to delete stock barang.'], 500);
                }

                DB::commit();

                $response = array(
                        "success" => 1,
                        "message" => 'Stock barang successfully deleted'
                    );

                return response()->json($response, 201);

            }catch(\Exception $e){
               DB::rollback();
               return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
            }
        }catch(\Exception $e){
           return response()->json(['success' => 0, 'message' => $e->getMessage()], 500);
        }
    }
}
