# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Kebutuhan untuk membangun project ini : ###

* Menggunakan framework laravel versi `^8.75` dan php version `8.0.14`
* MySQL
* Postman

### Untuk instalasi laravel pertama-tama : ###

* Clone project 
* Composer install
* Membuat DB terlebih dahulu untuk kebutuhan migrate
* Migrate DB dengan menggunakan perintah : php artisan migrate
* Generate Key dengan menggunakan perintah : php artisan key:generate
* Setting .env untuk nama database, username dan password nya

### Pengembangan : ###
* Menambahkan penjualan barang, jadi ketika melakukan penjualan maka stock barang akan otomatis berkurang

### Collection & Environment Postman : ###
* Collection Bisa di download di source ini tepatnya di file `Online Written Test.postman_collection.json` dan Environtment di file `Online_Written_Test.postman_environment.json`